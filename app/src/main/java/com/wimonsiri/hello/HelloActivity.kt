package com.wimonsiri.hello

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val  displayName: TextView =findViewById(R.id.textView4)
        val name: String=intent.getStringExtra("name").toString()
        displayName.text=name
    }
}