package com.wimonsiri.hello

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    val TAG="Hello"
    fun helloName(v: View?) {
        val name: TextView = findViewById(R.id.textView)
        val id:TextView = findViewById(R.id.textView2)
        Log.d(TAG,"Name: ${name.text} : ID: ${id.text}")
        val i = Intent(this, HelloActivity::class.java)
        i.putExtra("name",name.text)
        startActivity(i)
    }
}

